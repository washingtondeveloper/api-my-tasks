import express from 'express'

const Router = express.Router()

import TaskController from '../controllers/taskController'
import UsersController from '../controllers/usersController'
import LoginController from '../controllers/loginController'

/* Auth */
import authMiddleware from '../middlewares/authMiddleware'

/**
** Login
**/
Router.post('/login', LoginController.show)

/**
** Tasks
**/
Router.get('/tasks', authMiddleware, TaskController.index)
Router.post('/tasks', authMiddleware, TaskController.store)
Router.put('/tasks/:id', authMiddleware, TaskController.update)
Router.delete('/tasks/:id', authMiddleware, TaskController.delete)

/**
** Users
**/
Router.post('/users', UsersController.store)

export default Router