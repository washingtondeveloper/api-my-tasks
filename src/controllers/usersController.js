import User from '../models/User'
import generateToken from '../config/generateToken'


class UsersController {

	async store(req, res) {
		const user = req.body
		
		try {
			const userCreated = await User.create(user)
			const token = generateToken({ id: userCreated._id })

			userCreated.password = undefined

			return res.json({ user: userCreated, token })
		}catch(error) {
			throw new Error(error)
		}
	}
}

export default new UsersController()