import User from '../models/User'
import generateToken from '../config/generateToken'

import bcrypt from 'bcrypt'

class LoginController {

	async show(req, res) {
		const { email, password } = req.body

		const userFound = await User.findOne({ email })

		if(!userFound) 
			return res.status(404).json({ message: 'User not found'})

		if(!await bcrypt.compare(password, userFound.password))
			return res.status(401).json({ message: "Usuario ou senha inválido"})

		const token = generateToken({ id: userFound._id })

		userFound.password = undefined

		return res.json({ user:userFound, token })
	}
}

export default new LoginController()