import User from '../models/User';

class TaskController {
	async index(req, res) {
		const userid = req.userid;

		const userFound = await User.findById(userid);

		if (!userFound) res.status(404).json({ message: `Usuário não encontrado com id ${userid}` });

		return res.json(userFound.tasks);
	}

	async store(req, res) {
		const userid = req.userid;
		const task = req.body;

		if (!userid) res.json({ message: 'userid is required' });

		const userFound = await User.findById(userid);

		if (!userFound) res.json({ message: `User not found with id ${userid}` });

		if (!task.name.trim()) res.json({ message: 'Name is required' });
		if (!task.description.trim()) res.json({ message: 'Description is required' });

		userFound.tasks.push(task);

		try {
			const userChanged = await userFound.save();
			return res.status(201).json(userChanged.tasks);
		} catch (error) {
			return res.status(500).json({ error });
		}
	}

	async update(req, res) {
		const userid = req.userid;
		const { id: idTask } = req.params;
		const task = req.body;

		const userFound = await User.findById(userid);

		if (!userFound) res.json({ message: `User not found with id ${userid}` });

		userFound.tasks = userFound.tasks.map(
			(taskInter) => (taskInter._id == idTask ? { _id: idTask, ...task } : taskInter)
		);

		const taskChanged = await userFound.save();

		return res.json(taskChanged.tasks);
	}

	async delete(req, res) {
		const userid = req.userid;
		const { id: idTask } = req.params;

		const userFound = await User.findById(userid);

		if (!userFound) res.json({ message: `User not found with id ${userid}` });

		userFound.tasks = userFound.tasks.filter((tk) => tk._id != idTask);
		const userChanged = await userFound.save();

		return res.json(userChanged.tasks);
	}
}

export default new TaskController();
