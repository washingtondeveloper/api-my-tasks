import express from 'express';

const app = express();

require('./middlewares/configurations')(app);

export default app;
