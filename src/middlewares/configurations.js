import express from 'express'
import routes from '../routes'

import mongoose from 'mongoose'

module.exports = app => {
	mongoose.connect(
  		process.env.MONGO_TASKS,
	  {
	    useNewUrlParser: true,
	    useCreateIndex: true
	  }
	)

	const PORT = process.env.PORT || 3003
	app.use(express.json())
	app.use(routes)
	app.listen(PORT, () => console.log(`Server running on PORT ${PORT}`))	
}

