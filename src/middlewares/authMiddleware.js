import config from '../config'
import jwt from 'jsonwebtoken';

export default (req, res, next) => {
	const authHeader = req.headers.authorization;

	if(!authHeader)
		return res.status(401).json({ error: 'Token não informado' });

	const parts = authHeader.split(' ');

	const [bearer, token] = parts

	if(parts.length !== 2 || !/^Bearer$/i.test(bearer))
		return res.status(401).json({ error: 'Token com formato errado'})

	jwt.verify(token, config.secret, function(error, decoded) {
		if(error) {
			return res.status(401).json({ error: `Token inválido ${error.message}` })
		}

		req.userid = decoded.id
	})


	next();
}