import jwt from 'jsonwebtoken';
import config from '../config'

export default (params = {}) => {
	return jwt.sign({...params }, config.secret, { expiresIn: 86400 })
}