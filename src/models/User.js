import { Schema, model } from 'mongoose';
import bcrypt from 'bcrypt';

const UserSchema = new Schema(
	{
		name: {
			type: String,
			required: true
		},
		email: {
			type: String,
			required: true,
			unique: true,
			lowercase: true
		},
		password: {
			type: String,
			required: true
		},
		tasks: [
			{
				name: String,
				description: String,
				done: { type: Boolean, default: false }
			}
		]
	},
	{
		timestamps: true
	}
);

UserSchema.pre('save', async function(next) {
  const hash = await bcrypt.hash(this.password, 10)
  this.password = hash
  next();
});

export default model('User', UserSchema);
